<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JobUserHirer
 * @package App\Models
 * @version May 24, 2019, 11:04 pm UTC
 *
 * @property integer user_id
 * @property integer job_id
 * @property integer category_id
 */
class JobUserHirer extends Model
{
    use SoftDeletes;

    public $table = 'job_user_hirer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'job_id',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'job_id' => 'integer',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'job_id' => 'required',
        'category_id' => 'required'
    ];

    
}
