<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version May 24, 2019, 11:07 pm UTC
 *
 * @property string name
 * @property string email
 * @property string phone
 * @property string password
 * @property string avatar
 * @property string facebook_profile
 * @property string linkedin_profile
 * @property string gender
 * @property integer role_id
 * @property string remember_token
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'avatar',
        'facebook_profile',
        'linkedin_profile',
        'gender',
        'role_id',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'password' => 'string',
        'avatar' => 'string',
        'facebook_profile' => 'string',
        'linkedin_profile' => 'string',
        'gender' => 'string',
        'role_id' => 'integer',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'role_id' => 'required'
    ];

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }
}
