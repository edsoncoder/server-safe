<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version May 24, 2019, 11:01 pm UTC
 *
 * @property string category_id
 * @property string name
 * @property string description
 * @property string link_test
 * @property string link_post
 * @property integer views
 * @property integer value
 * @property integer status
 * @property integer user_id_coder
 * @property integer user_id_hirer
 */
class Job extends Model
{
    use SoftDeletes;

    public $table = 'jobs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'category_id',
        'name',
        'description',
        'link_test',
        'link_post',
        'views',
        'value',
        'status',
        'user_id_coder',
        'user_id_hirer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'category_id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'link_test' => 'string',
        'link_post' => 'string',
        'views' => 'integer',
        'value' => 'integer',
        'status' => 'integer',
        'user_id_coder' => 'integer',
        'user_id_hirer' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id_coder' => 'required'
    ];

    
}
