<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	 /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
 	public function redirectToProvider()
    {
         //check if its localhost
         $blackList = array('127.0.0.1', "::1");
         $ip=$_SERVER["REMOTE_ADDR"];
         if(in_array($ip,$blackList)){
            $erro="ip [{$ip}] reconhecido como local.";
            If ($_SERVER['SERVER_PORT'] != 443 ) {
                $erro.="Falha ao confirmar certificado SSL!";
            }     
            dd($erro);
         }else{
            return Socialite::driver('facebook')->redirect();
         }
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        
        $userSocial = Socialite::driver('facebook')->user(); 
        //fix erro on refresh page

        //Check if user exists
        $user = User::where('email',$userSocial->user['email'])->first();
        
        if($user){
        	if(Auth::loginUsingId($user->id)){
            	return redirect()->route('home');
            }
        }
        //dd($userSocial);
        //user sign up by facebook
        $userSignup = User::create([
            'name' => $userSocial->user['name'],
            'email' => $userSocial->user['email'],
            'password' => bcrypt('1234'),
            'avatar' => $userSocial->avatar,
            'facebook_profile' => $userSocial->user['id']
        ]);
    	if($userSignup){
        	if(Auth::loginUsingId($userSignup->id)){
            	return redirect()->route('home');
            }
        }
        
    }
}
