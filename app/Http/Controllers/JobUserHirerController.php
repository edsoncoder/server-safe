<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobUserHirerRequest;
use App\Http\Requests\UpdateJobUserHirerRequest;
use App\Repositories\JobUserHirerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class JobUserHirerController extends AppBaseController
{
    /** @var  JobUserHirerRepository */
    private $jobUserHirerRepository;

    public function __construct(JobUserHirerRepository $jobUserHirerRepo)
    {
        $this->jobUserHirerRepository = $jobUserHirerRepo;
    }

    /**
     * Display a listing of the JobUserHirer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->jobUserHirerRepository->pushCriteria(new RequestCriteria($request));
        $jobUserHirers = $this->jobUserHirerRepository->all();

        return view('job_user_hirers.index')
            ->with('jobUserHirers', $jobUserHirers);
    }

    /**
     * Show the form for creating a new JobUserHirer.
     *
     * @return Response
     */
    public function create()
    {
        return view('job_user_hirers.create');
    }

    /**
     * Store a newly created JobUserHirer in storage.
     *
     * @param CreateJobUserHirerRequest $request
     *
     * @return Response
     */
    public function store(CreateJobUserHirerRequest $request)
    {
        $input = $request->all();

        $jobUserHirer = $this->jobUserHirerRepository->create($input);

        Flash::success('Job User Hirer saved successfully.');

        return redirect(route('jobUserHirers.index'));
    }

    /**
     * Display the specified JobUserHirer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobUserHirer = $this->jobUserHirerRepository->findWithoutFail($id);

        if (empty($jobUserHirer)) {
            Flash::error('Job User Hirer not found');

            return redirect(route('jobUserHirers.index'));
        }

        return view('job_user_hirers.show')->with('jobUserHirer', $jobUserHirer);
    }

    /**
     * Show the form for editing the specified JobUserHirer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobUserHirer = $this->jobUserHirerRepository->findWithoutFail($id);

        if (empty($jobUserHirer)) {
            Flash::error('Job User Hirer not found');

            return redirect(route('jobUserHirers.index'));
        }

        return view('job_user_hirers.edit')->with('jobUserHirer', $jobUserHirer);
    }

    /**
     * Update the specified JobUserHirer in storage.
     *
     * @param  int              $id
     * @param UpdateJobUserHirerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobUserHirerRequest $request)
    {
        $jobUserHirer = $this->jobUserHirerRepository->findWithoutFail($id);

        if (empty($jobUserHirer)) {
            Flash::error('Job User Hirer not found');

            return redirect(route('jobUserHirers.index'));
        }

        $jobUserHirer = $this->jobUserHirerRepository->update($request->all(), $id);

        Flash::success('Job User Hirer updated successfully.');

        return redirect(route('jobUserHirers.index'));
    }

    /**
     * Remove the specified JobUserHirer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobUserHirer = $this->jobUserHirerRepository->findWithoutFail($id);

        if (empty($jobUserHirer)) {
            Flash::error('Job User Hirer not found');

            return redirect(route('jobUserHirers.index'));
        }

        $this->jobUserHirerRepository->delete($id);

        Flash::success('Job User Hirer deleted successfully.');

        return redirect(route('jobUserHirers.index'));
    }
}
