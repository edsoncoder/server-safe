<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobUserCoderRequest;
use App\Http\Requests\UpdateJobUserCoderRequest;
use App\Repositories\JobUserCoderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class JobUserCoderController extends AppBaseController
{
    /** @var  JobUserCoderRepository */
    private $jobUserCoderRepository;

    public function __construct(JobUserCoderRepository $jobUserCoderRepo)
    {
        $this->jobUserCoderRepository = $jobUserCoderRepo;
    }

    /**
     * Display a listing of the JobUserCoder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->jobUserCoderRepository->pushCriteria(new RequestCriteria($request));
        $jobUserCoders = $this->jobUserCoderRepository->all();

        return view('job_user_coders.index')
            ->with('jobUserCoders', $jobUserCoders);
    }

    /**
     * Show the form for creating a new JobUserCoder.
     *
     * @return Response
     */
    public function create()
    {
        return view('job_user_coders.create');
    }

    /**
     * Store a newly created JobUserCoder in storage.
     *
     * @param CreateJobUserCoderRequest $request
     *
     * @return Response
     */
    public function store(CreateJobUserCoderRequest $request)
    {
        $input = $request->all();

        $jobUserCoder = $this->jobUserCoderRepository->create($input);

        Flash::success('Job User Coder saved successfully.');

        return redirect(route('jobUserCoders.index'));
    }

    /**
     * Display the specified JobUserCoder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobUserCoder = $this->jobUserCoderRepository->findWithoutFail($id);

        if (empty($jobUserCoder)) {
            Flash::error('Job User Coder not found');

            return redirect(route('jobUserCoders.index'));
        }

        return view('job_user_coders.show')->with('jobUserCoder', $jobUserCoder);
    }

    /**
     * Show the form for editing the specified JobUserCoder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobUserCoder = $this->jobUserCoderRepository->findWithoutFail($id);

        if (empty($jobUserCoder)) {
            Flash::error('Job User Coder not found');

            return redirect(route('jobUserCoders.index'));
        }

        return view('job_user_coders.edit')->with('jobUserCoder', $jobUserCoder);
    }

    /**
     * Update the specified JobUserCoder in storage.
     *
     * @param  int              $id
     * @param UpdateJobUserCoderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobUserCoderRequest $request)
    {
        $jobUserCoder = $this->jobUserCoderRepository->findWithoutFail($id);

        if (empty($jobUserCoder)) {
            Flash::error('Job User Coder not found');

            return redirect(route('jobUserCoders.index'));
        }

        $jobUserCoder = $this->jobUserCoderRepository->update($request->all(), $id);

        Flash::success('Job User Coder updated successfully.');

        return redirect(route('jobUserCoders.index'));
    }

    /**
     * Remove the specified JobUserCoder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobUserCoder = $this->jobUserCoderRepository->findWithoutFail($id);

        if (empty($jobUserCoder)) {
            Flash::error('Job User Coder not found');

            return redirect(route('jobUserCoders.index'));
        }

        $this->jobUserCoderRepository->delete($id);

        Flash::success('Job User Coder deleted successfully.');

        return redirect(route('jobUserCoders.index'));
    }
}
