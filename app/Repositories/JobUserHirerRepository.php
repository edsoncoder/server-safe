<?php

namespace App\Repositories;

use App\Models\JobUserHirer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JobUserHirerRepository
 * @package App\Repositories
 * @version May 24, 2019, 11:04 pm UTC
 *
 * @method JobUserHirer findWithoutFail($id, $columns = ['*'])
 * @method JobUserHirer find($id, $columns = ['*'])
 * @method JobUserHirer first($columns = ['*'])
*/
class JobUserHirerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'job_id',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobUserHirer::class;
    }
}
