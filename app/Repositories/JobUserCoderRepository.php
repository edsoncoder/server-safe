<?php

namespace App\Repositories;

use App\Models\JobUserCoder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JobUserCoderRepository
 * @package App\Repositories
 * @version May 24, 2019, 11:04 pm UTC
 *
 * @method JobUserCoder findWithoutFail($id, $columns = ['*'])
 * @method JobUserCoder find($id, $columns = ['*'])
 * @method JobUserCoder first($columns = ['*'])
*/
class JobUserCoderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'job_id',
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobUserCoder::class;
    }
}
