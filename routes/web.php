<?php
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckRole;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//facebook login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('login.facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
//dd();
Route::middleware(['auth'])->group( function () {
    Route::get('users/{id}', 'UserController@show');
    Route::resource('jobs', 'JobController');
    Route::resource('jobUserCoders', 'JobUserCoderController');
    Route::resource('jobUserHirers', 'JobUserHirerController');
    Route::get('users/{id}/edit', 'UserController@edit');
    Route::put('users/{id}', 'UserController@update');
    Route::patch('users/{id}', 'UserController@update');
});
Route::middleware(['checkRole'])->group( function () {
    //only admin can access this route
    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');
    Route::resource('roles', 'RoleController');
    Route::resource('settings', 'SettingController');
});


