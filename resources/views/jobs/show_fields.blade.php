<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $job->id !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $job->category_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $job->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $job->description !!}</p>
</div>

<!-- Link Test Field -->
<div class="form-group">
    {!! Form::label('link_test', 'Link Test:') !!}
    <p>{!! $job->link_test !!}</p>
</div>

<!-- Link Post Field -->
<div class="form-group">
    {!! Form::label('link_post', 'Link Post:') !!}
    <p>{!! $job->link_post !!}</p>
</div>

<!-- Views Field -->
<div class="form-group">
    {!! Form::label('views', 'Views:') !!}
    <p>{!! $job->views !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $job->value !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $job->status !!}</p>
</div>

<!-- User Id Coder Field -->
<div class="form-group">
    {!! Form::label('user_id_coder', 'User Id Coder:') !!}
    <p>{!! $job->user_id_coder !!}</p>
</div>

<!-- User Id Hirer Field -->
<div class="form-group">
    {!! Form::label('user_id_hirer', 'User Id Hirer:') !!}
    <p>{!! $job->user_id_hirer !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $job->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $job->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $job->deleted_at !!}</p>
</div>

