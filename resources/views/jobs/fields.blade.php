<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Test Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link_test', 'Link Test:') !!}
    {!! Form::text('link_test', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Post Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link_post', 'Link Post:') !!}
    {!! Form::text('link_post', null, ['class' => 'form-control']) !!}
</div>

<!-- Views Field -->
<div class="form-group col-sm-6">
    {!! Form::label('views', 'Views:') !!}
    {!! Form::number('views', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::number('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Coder Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id_coder', 'User Id Coder:') !!}
    {!! Form::number('user_id_coder', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Hirer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id_hirer', 'User Id Hirer:') !!}
    {!! Form::number('user_id_hirer', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
</div>
