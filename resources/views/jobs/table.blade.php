<div class="table-responsive">
    <table class="table" id="jobs-table">
        <thead>
            <tr>
                <th>Category Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Link Test</th>
        <th>Link Post</th>
        <th>Views</th>
        <th>Value</th>
        <th>Status</th>
        <th>User Id Coder</th>
        <th>User Id Hirer</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jobs as $job)
            <tr>
                <td>{!! $job->category_id !!}</td>
            <td>{!! $job->name !!}</td>
            <td>{!! $job->description !!}</td>
            <td>{!! $job->link_test !!}</td>
            <td>{!! $job->link_post !!}</td>
            <td>{!! $job->views !!}</td>
            <td>{!! $job->value !!}</td>
            <td>{!! $job->status !!}</td>
            <td>{!! $job->user_id_coder !!}</td>
            <td>{!! $job->user_id_hirer !!}</td>
                <td>
                    {!! Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('jobs.show', [$job->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('jobs.edit', [$job->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
