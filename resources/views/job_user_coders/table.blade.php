<div class="table-responsive">
    <table class="table" id="jobUserCoders-table">
        <thead>
            <tr>
                <th>User Id</th>
        <th>Job Id</th>
        <th>Category Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jobUserCoders as $jobUserCoder)
            <tr>
                <td>{!! $jobUserCoder->user_id !!}</td>
            <td>{!! $jobUserCoder->job_id !!}</td>
            <td>{!! $jobUserCoder->category_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['jobUserCoders.destroy', $jobUserCoder->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('jobUserCoders.show', [$jobUserCoder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('jobUserCoders.edit', [$jobUserCoder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
