
<li class="{{ Request::is('jobs*') ? 'active' : '' }}">
    <a href="{!! route('jobs.index') !!}"><i class="fa fa-edit"></i><span>Jobs</span></a>
</li>
<li class="{{ Request::is('jobUserCoders*') ? 'active' : '' }}">
    <a href="{!! route('jobUserCoders.index') !!}"><i class="fa fa-edit"></i><span>Programmers</span></a>
</li>

<li class="{{ Request::is('jobUserHirers*') ? 'active' : '' }}">
    <a href="{!! route('jobUserHirers.index') !!}"><i class="fa fa-edit"></i><span>Hirers</span></a>
</li>
@if(Auth::user()->role_id == 2)
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>
@endif
@if( Auth::user()->role_id == 2 )
<li class="{{ Request::is('categories*') ? 'active' : '' }}">
    <a href="{!! route('categories.index') !!}"><i class="fa fa-edit"></i><span>Categories</span></a>
</li>
@endif

@if(Auth::user()->role_id == 2)
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>
@endif
@if(Auth::user()->role_id == 2)
<li class="{{ Request::is('settings*') ? 'active' : '' }}">
    <a href="{!! route('settings.index') !!}"><i class="fa fa-edit"></i><span>Settings</span></a>
</li>
@endif
