<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="col-md-6">
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="{!! $user->avatar !!}" alt="{!! $user->name !!}">
              </div>
              <!-- /.widget-user-image -->
              <h1 class="widget-user-username"> <b>{!! $user->name !!}</b></h1>
              <h4 class="widget-user-desc">Gender: {!! $user->gender !!}</h4>
              <h4 class="widget-user-desc">Role: {!! $user->role['name'] !!}</h4>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>
              </ul>
            </div>
            </div>
            </div>
            <div class="col-md-6">
             <h3 class="widget-user-username">Contatos</h3>
              <h5 class="widget-user-desc">Email: {!! $user->email !!}</h5>
              <h6 class="widget-user-desc">Phone: {!! $user->phone !!}</h6>
              <h6 class="widget-user-desc">Facebook: {!! $user->facebook_profile!!}</h6>
              <h6 class="widget-user-desc">Linkedin: {!! $user->linkedin_profile !!}</h6>
            </div>
            
          </div>


