@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job User Hirer
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'jobUserHirers.store']) !!}

                        @include('job_user_hirers.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
