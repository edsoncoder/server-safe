<div class="table-responsive">
    <table class="table" id="jobUserHirers-table">
        <thead>
            <tr>
                <th>User Id</th>
        <th>Job Id</th>
        <th>Category Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jobUserHirers as $jobUserHirer)
            <tr>
                <td>{!! $jobUserHirer->user_id !!}</td>
            <td>{!! $jobUserHirer->job_id !!}</td>
            <td>{!! $jobUserHirer->category_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['jobUserHirers.destroy', $jobUserHirer->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('jobUserHirers.show', [$jobUserHirer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('jobUserHirers.edit', [$jobUserHirer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
