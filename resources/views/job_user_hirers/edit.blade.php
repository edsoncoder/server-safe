@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job User Hirer
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($jobUserHirer, ['route' => ['jobUserHirers.update', $jobUserHirer->id], 'method' => 'patch']) !!}

                        @include('job_user_hirers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection