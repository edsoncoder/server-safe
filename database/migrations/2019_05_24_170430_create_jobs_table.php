<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('domain')->nullable();
            $table->string('link_test')->nullable();
            $table->string('link_post')->nullable();
            $table->integer('views')->nullable();
            $table->integer('value')->nullable();
            $table->integer('status')->nullable();
            $table->integer('user_id_coder');
            $table->integer('user_id_hirer')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
